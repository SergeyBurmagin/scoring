package com.service;

import com.dto.Scoring;
import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ScoringService {


    @Autowired
    private DmnEngine dmnEngine;
    @Autowired
    @Qualifier(value = "scoring")
    private DmnDecision decision;

    public boolean getResult(Scoring scoring) {

        VariableMap variables = Variables.createVariables()
                .putValue("egrul", scoring.getEgrul())
                .putValue("clientNalog", scoring.getClientNalog())
                .putValue("eaisExsist", scoring.getEaisExsist())
                .putValue("country", scoring.getCountry())
                .putValue("sum", scoring.getSum());

        DmnDecisionTableResult dishDecisionResult = dmnEngine.evaluateDecisionTable(decision, variables);

        List<Map<String, Object>> list =  dishDecisionResult.getResultList();

        DmnDecisionTableResult result = dmnEngine.evaluateDecisionTable(decision, variables);

        return ((Boolean) result.getSingleResult().get("efterlon")).booleanValue();
    }
}
