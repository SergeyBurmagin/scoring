package com.controller;

import com.dto.Scoring;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.service.ScoringService;

import static org.springframework.http.HttpStatus.OK;

@RestController
@Slf4j
@RequestMapping("/api")
public class ScoringController {

    @Autowired
    ScoringService scoringService;

    @PostMapping("/scoring")
    public ResponseEntity getResult(
            @RequestBody final Scoring scoring
    ) {

        return ResponseEntity.ok(scoringService.getResult(scoring));
        }
}
