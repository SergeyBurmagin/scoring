package com.config;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.PropertySource;

@PropertySource({"classpath:application.properties"})
public class BaseApplication {
    public BaseApplication() {
    }

    public static void run(String[] args, Class<?> applicationConfigClass) {
        SpringApplication.run(applicationConfigClass, args);
    }
}
