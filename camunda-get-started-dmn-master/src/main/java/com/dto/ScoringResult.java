package com.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScoringResult {

    private Boolean egrul;
    private Boolean clientNalog;
    private Boolean eaisExsist;
    private Boolean country;
    private Boolean sum;
}
