package com.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Scoring implements Serializable {

    private Integer egrul;
    private Integer clientNalog;
    private Integer eaisExsist;
    private Integer country;
    private Integer sum;

}
