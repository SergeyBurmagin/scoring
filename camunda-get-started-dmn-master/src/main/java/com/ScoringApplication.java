package com;

import com.config.BaseApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@SpringBootApplication
@Configuration
@ComponentScan({"com"})
public class ScoringApplication extends BaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScoringApplication.class, args);
    }

}
